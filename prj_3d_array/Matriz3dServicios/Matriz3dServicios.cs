﻿using Model;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class Matriz3dServicios
    {

        private readonly Matriz3dRepository repo;
        public Matriz3dServicios()
        {
            repo = new Matriz3dRepository();
        }

        public RespuestaDTO Procesar(string[] parametros)
        {
            return repo.EjecutarProceso(parametros);
        }
    }
}
