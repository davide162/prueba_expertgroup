﻿using Model;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace prj_3d_array.Controllers
{
    public class HomeController : Controller
    {
        private readonly Matriz3dServicios matrizService;
        public HomeController()
        {
            matrizService = new Matriz3dServicios();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public JsonResult EjecutarProceso(string data_input)
        {
            string[] parametros = Regex.Split(data_input, "\r\n|\r|\n");

            RespuestaDTO result=matrizService.Procesar(parametros);
            return Json(result.salida);
        }


    }
}