﻿using System.Web;
using System.Web.Mvc;

namespace prj_3d_array
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
