﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class Matriz3dRepository
    {
        public Matriz3D array3d = null;
        public RespuestaDTO EjecutarProceso(string[] parametros)
        {
            //Matriz3D array3d = null;
            int casosDePrueba = 0;
            int tamanoMatriz = 0;
            int numeroOperaciones = 0;
            List<int> salida = new List<int>();
            if (parametros.Length > 0)
            {
                casosDePrueba = int.Parse(parametros[0]);
            }
            for (int t = 1; t < casosDePrueba; t++)
            {
                foreach (string item in parametros)
                {
                    string[] temp = item.Split(' ');

                    if (temp.Length == 2)
                    {
                        if (array3d == null)
                        {
                            tamanoMatriz = int.Parse(temp[0]);
                            numeroOperaciones = int.Parse(temp[1]);
                            array3d = CargarDatos(casosDePrueba, tamanoMatriz, numeroOperaciones);
                        }
                    }
                    else if (temp.Length == 5)
                    {
                        int s_temp = UpdateExecute(new UpdateDTO
                        {
                            Coor_X = int.Parse(temp[1]) - 1,
                            Coor_Y = int.Parse(temp[2]) - 1,
                            Coor_Z = int.Parse(temp[3]) - 1,
                            Valor = int.Parse(temp[4]),
                        });
                        salida.Add(s_temp);
                    }
                    else if (temp.Length == 7)
                    {
                        int s_temp = QueryExecute(new QueryDTO
                        {
                            Coor_X1 = int.Parse(temp[1]) - 1,
                            Coor_X2 = int.Parse(temp[4]) - 1,
                            Coor_Y1 = int.Parse(temp[2]) - 1,
                            Coor_Y2 = int.Parse(temp[5]) - 1,
                            Coor_Z1 = int.Parse(temp[3]) - 1,
                            Coor_Z2 = int.Parse(temp[6]) - 1,
                        });
                        salida.Add(s_temp);
                    }
                }
            }
            return new RespuestaDTO { id = 1, salida = string.Join(",", salida) };
        }
        private Matriz3D CargarDatos(int casosDePrueba, int tamanoMatriz, int numeroOperaciones)
        {
            Matriz3D array3d = new Matriz3D(tamanoMatriz);
            array3d.CasosDePrueba = casosDePrueba;
            array3d.NumeroOperaciones = numeroOperaciones;
            array3d.TamanoMatriz = tamanoMatriz;
            return array3d;
        }

        private int UpdateExecute(UpdateDTO input)
        {
            int result = 0;
            if (input.Coor_X >= 0 && input.Coor_Y >= 0 && input.Coor_Z >= 0
                && array3d.array3D.GetLength(0) > input.Coor_X && array3d.array3D.GetLength(1) > input.Coor_Y && array3d.array3D.GetLength(2) > input.Coor_Z)
            {
                int temp = array3d.array3D[input.Coor_X, input.Coor_Y, input.Coor_Z];
                result = temp + input.Valor;
                array3d.array3D[input.Coor_X, input.Coor_Y, input.Coor_Z] = result;
            }
            return result;
        }

        private int QueryExecute(QueryDTO input)
        {
            int result = 0;
            if (input.Coor_X1 >= 0 && input.Coor_Y1 >= 0 && input.Coor_Z1 >= 0 && input.Coor_X2 >= 0 && input.Coor_Y2 >= 0 && input.Coor_Z2 >= 0
                && array3d.array3D.GetLength(0) > input.Coor_X1 && array3d.array3D.GetLength(1) > input.Coor_Y1 && array3d.array3D.GetLength(2) > input.Coor_Z1)
            {
                for (int i = input.Coor_X1; i < input.Coor_X2; i++)
                {
                    for (int j = input.Coor_Y1; j < input.Coor_Y2; j++)
                    {
                        for (int k = input.Coor_Z1; k < input.Coor_Z2; k++)
                        {
                            result += array3d.array3D[i, j, k];
                        }
                    }
                }
            }
            return result;
        }


    }
}
