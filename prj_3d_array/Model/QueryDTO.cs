﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class QueryDTO
    {
        public int Coor_X1 { get; set; }
        public int Coor_X2 { get; set; }
        public int Coor_Y1 { get; set; }
        public int Coor_Y2 { get; set; }
        public int Coor_Z1 { get; set; }
        public int Coor_Z2 { get; set; }

    }
}
