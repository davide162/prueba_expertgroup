﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Matriz3D
    {
        public int[,,] array3D { get; set; }
        public int CasosDePrueba { get; set; }
        public int TamanoMatriz { get; set; }
        public int NumeroOperaciones { get; set; }

        public Matriz3D(int tamano)
        {
            this.array3D = new int[tamano, tamano, tamano];
        }
    }
}
