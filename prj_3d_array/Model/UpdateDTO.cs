﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class UpdateDTO
    {
        public int Coor_X { get; set; }
        public int Coor_Y { get; set; }
        public int Coor_Z { get; set; }
        public int Valor { get; set; }
    }
}
